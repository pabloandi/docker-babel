FROM ubuntu:trusty
MAINTAINER Pablo Andrés Díaz <pabloandi@gmail.com>

ADD indexdata.asc indexdata.asc

RUN echo "deb http://ftp.indexdata.dk/ubuntu trusty main" >> /etc/apt/sources.list && echo "deb-src http://ftp.indexdata.dk/ubuntu trusty main" >> /etc/apt/sources.list
RUN apt-key add indexdata.asc
RUN apt-get update && apt-get -y install php5 php5-yaz php5-redis curl php5-mcrypt php5-json && apt-get -y autoremove && apt-get clean

COPY 000-default.conf /etc/apache2/sites-enabled/
COPY default-ssl /etc/apache2/sites-enabled/

RUN /usr/sbin/a2enmod rewrite
RUN php5enmod mcrypt
RUN echo "extension=yaz.so" >> /etc/php5/apache2/php.ini

RUN chown -R www-data:www-data /var/www/

EXPOSE 80
EXPOSE 443

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
